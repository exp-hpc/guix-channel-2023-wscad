(define-module (pajeng)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages pretty-print)
  #:use-module (guix git-download)
  #:use-module (gnu packages tex)
  #:use-module (gnu packages ghostscript)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages documentation)
)

(define-public pajeng
(package
 (name "pajeng")
 (version "1.3.6")
 (source
   (origin
      (method url-fetch)
      (uri
        (string-append
          "https://github.com/schnorr/pajeng/archive/refs/tags/"
          version
          ".tar.gz"))
      (sha256
         (base32
           "1cyhp6lgx7w3qw0pxybj26h4pwfv5rcw5vz8p5zl7imhmszj49qs"
          ))))
 (build-system cmake-build-system)
 (arguments
  `(#:tests? #f
    #:validate-runpath? #f)
 )
 (inputs (list
            boost
            bison
            flex
         ))
 (synopsis
    "PajeNG: library and associated tools for Paje trace files")
 (description
    "PajeNG is a re-implementation of the well-known Paje
    visualization tool for the analysis of execution traces.
    PajeNG comprises the libpaje library, and an auxiliary tool
    called pj_dump to transform Paje trace files to
    Comma-Separated Value (CSV). The space-time visualization
    tool called pajeng had been deprecated (removed from the
    sources) since modern tools do a better job (see
    pj_gantt).")
 (home-page "https://github.com/schnorr/pajeng")
 (license gpl3))
)

(define-public pajeng-develop
(let ((commit "ca24c95cc5b4e53455058e180f01d5a5febccac6")
      (revision "1"))
(package (inherit pajeng)
  (name "pajeng")
  (version (git-version "1.3.6" revision commit))
   (source
     (origin
       (method git-fetch)
       (uri (git-reference
         (url (string-append
                "https://github.com/schnorr/"
                name))
                   (commit commit)))
       (sha256
         (base32
          "1c1ggfgdl5xxq8jkvf774440j5lyn37n8qll354d4lbqxm81v9av")
        )
      )
    )
   (inputs (modify-inputs (package-inputs pajeng)
                    (prepend fmt)))
  )
)
)

(define-public pajeng-doc
(package (inherit pajeng)
   (name "pajeng-doc")
   (build-system cmake-build-system)
   (arguments
     '(#:configure-flags '("-DPAJE_DOC=ON")))
   (inputs (modify-inputs (package-inputs pajeng)
			   (prepend texlive-scheme-basic ghostscript poppler asciidoc texlive-titlesec texlive-setspace texlive-listings texlive-gsftopk)))
)
)
